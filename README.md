# queuectl

Program to bulk-load items into redis queues.

Call it using `./queuectl -h` to see the instructions.
Note that the default redis url assumes the primary redis on blackbird is available on localhost using the default redis port.

Note that there is an environment variable set on blackbird that will make it just work there as well.

## Features
* Retrieve files from URLs
* ZSTD Decompression
* XZ Decompression
* GZIP Decompression
* Progress bar (can be disabled)

## Building
Install rust via rustup.
Enable the `x86_64-unknown-linux-musl` target architecture.
```sh
rustup target add x86_64-unknown-linux-musl
```
Now build the binary using:
```sh
cargo build --release --target=x86_64-unknown-linux-musl
```

The binary is now at `target/x86_64-unknown-linux-musl/release/queuectl`.
This is a static binary that can be copied around at will.
