use clap::Parser;
use once_cell::sync::Lazy;

#[derive(Parser)]
#[clap(name = "queuectl")]
#[clap(version = "1.0")]
pub struct Cli {
    #[clap(short, long)]
    /// Input path. Can be a file or a url that starts with http:// or https://.
    pub input: String,
    #[clap(short, long)]
    /// Tracker slug for the project.
    pub project: String,
    #[clap(short, long, default_value = "todo")]
    /// The queue where items will be queued into.
    pub queue: String,
    #[clap(long, default_value = "8192")]
    /// How many items to send per SADD command.
    pub chunk_size: usize,
    #[clap(short, long, default_value = "redis://127.0.0.1/", env = "TRACKER_REDIS_URL")]
    /// URL of the redis to connect to.
    pub redis: String,
    #[clap(long, short, default_value = "auto", arg_enum)]
    /// Specify the compression of the input file. By default will autodetect by file extension.
    pub compression: crate::compression::CompressionMode,
    #[clap(long, default_value = "32")]
    /// How many commands to pipeline into redis.
    pub pipeline_size: usize,
    #[clap(long)]
    /// Do not show a progress bar.
    pub no_progressbar: bool,
}

pub static ARGS: Lazy<Cli> = Lazy::new(|| Cli::parse());
