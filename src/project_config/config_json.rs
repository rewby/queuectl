use serde::{Deserialize, Serialize};

#[derive(Default, Debug, Clone, PartialEq, Serialize, Deserialize)]
pub struct Root {
    pub max_claims_soft: Option<i64>,
    pub max_claims_hard: Option<i64>,
    pub moving_average_interval: Option<i64>,
    pub min_script_version: Option<String>,
    pub title: String,
    pub ignore_global_blocked: bool,
    pub item_type: String,
    pub history_length: i64,
    pub domains: Option<serde_json::Value>,
    pub valid_item_regexp: String,
    pub redis: Option<Redis>,
}

#[derive(Default, Debug, Clone, PartialEq, Serialize, Deserialize)]
pub struct Redis {
    pub host: String,
    pub port: u16,
    pub pass: String,
}
