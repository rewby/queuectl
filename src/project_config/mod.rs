use crate::args::ARGS;
use anyhow::Context;
use redis::AsyncCommands;
use tracing::info;
use url::Url;

pub mod config_json;

pub async fn get_redis_url() -> anyhow::Result<Url> {
    let base_redis_url = Url::parse(&ARGS.redis).context("parsing redis url")?;
    info!(
        "Connecting to redis {} to find project config...",
        base_redis_url
    );
    let client = redis::Client::open(base_redis_url.clone()).context("connecting to redis")?;
    let mut base_con = client
        .get_async_connection()
        .await
        .context("get_async_connection")?;
    info!("Connected!");

    info!("Attempting to retrieve project configuration...");
    let config: Option<String> = base_con
        .hget("trackers", &ARGS.project)
        .await
        .context("hget trackers")?;
    if config.is_none() {
        panic!("Unable to get project config!");
    }

    let config: config_json::Root =
        serde_json::from_str(&config.unwrap()).context("parsing project config")?;
    info!("Read config:\n{:#?}", config);

    if let Some(r) = config.redis {
        let mut u = Url::parse("redis://127.0.0.1/")?;
        u.set_host(Some(&r.host)).unwrap();
        u.set_port(Some(r.port)).unwrap();
        u.set_username("default").unwrap();
        u.set_password(Some(&r.pass)).unwrap();
        info!("Found project redis server at {}!", u);
        Ok(u)
    } else {
        info!("No project-specific redis config found; staying on this redis!");
        Ok(base_redis_url)
    }
}
